FROM node:14.14.0-alpine3.12

# Create app directory
RUN mkdir -p /app
WORKDIR /app

COPY node_modules/ node_modules/
COPY package.json ./
COPY entrypoint.sh ./
COPY lib/ lib/
COPY controllers/ controllers/
COPY *.js ./


ENTRYPOINT [ "/app/entrypoint.sh" ]