async function getConfigFromSecretManager(name) {
    const { SecretManagerServiceClient } = require('@google-cloud/secret-manager')
    const fs = require('fs').promises
    const client = new SecretManagerServiceClient()

    const [version] = await client.accessSecretVersion({ name: name })
    const payload = version.payload.data.toString()

    await fs.writeFile('config.production.json', payload)
    console.log("Written to config.production.json")
}

const secretVersionName = process.env.SECRET_VERSION_NAME
if (!secretVersionName) {
    console.error('Missing environment variable SECRET_VERSION_NAME')
    process.exit(1)
}

getConfigFromSecretManager(secretVersionName).catch((err) => {
    console.error(err)
    process.exit(1)
})